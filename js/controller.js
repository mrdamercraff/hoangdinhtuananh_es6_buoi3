export let layThongTinTuForm = () => {
  let job = document.getElementById("newTask").value;
  document.getElementById("newTask").value = "";
  return { job };
};
export let renderArr = (list) => {
  let innerHTML = "";
  let count = 0;
  list.forEach((item) => {
    count++;
    item.ma = count;
    innerHTML += `              <li>
    <p>${item.job}</p>
    <div class="buttons">
      <button class="remove" onclick="deleteCongViec(${item.ma})">
        <i class="fa fa-trash-alt"></i>
      </button>
      <button onclick="hoanThanhCongViec(${item.ma})">
        <i class="fa fa-check-circle complete"></i>
      </button>
    </div>
  </li>`;
  });
  document.getElementById("todo").innerHTML = innerHTML;
};
export let renderArrCVHT = (list) => {
  let innerHTML = "";
  let count = 0;
  list.forEach((item) => {
    count++;
    item.ma = count;
    innerHTML += `              <li>
    <p>${item.job}</p>
    <div class="buttons">
      <button class="remove" onclick="deleteCongViecHoanThanh(${item.ma})">
        <i class="fa fa-trash-alt"></i>
      </button>
      <button class="complete">
        <span><i class="fa fa-check-circle"></i></span>
      </button>
    </div>
  </li>`;
  });
  document.getElementById("completed").innerHTML = innerHTML;
};
