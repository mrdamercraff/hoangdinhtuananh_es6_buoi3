import { layThongTinTuForm, renderArr, renderArrCVHT } from "./controller.js";
import { viecCanLam } from "./modal.js";
const DSCV = [];
const CVHT = [];
let renderCongViec = () => {
  let thongTin = layThongTinTuForm();
  let { job } = thongTin;
  let congViec = new viecCanLam(job);
  DSCV.push(congViec);
  renderArr(DSCV);
};
document.getElementById("addItem").addEventListener("click", function () {
  renderCongViec();
  console.log(DSCV);
});
let deleteCongViec = (id) => {
  for (let i = 0; i < DSCV.length; i++) {
    if (DSCV[i].ma == id) {
      var congviec = DSCV.splice(i, 1);
    }
  }
  renderArr(DSCV);
  return congviec;
};
window.deleteCongViec = deleteCongViec;
let hoanThanhCongViec = (id) => {
  let thongTin = deleteCongViec(id);

  let congViec = new viecCanLam(thongTin[0].job);
  CVHT.push(congViec);
  renderArrCVHT(CVHT);
};
window.hoanThanhCongViec = hoanThanhCongViec;
let deleteCongViecHoanThanh = (id) => {
  for (let i = 0; i < CVHT.length; i++) {
    if (CVHT[i].ma == id) {
      CVHT.splice(i, 1);
    }
  }
  renderArrCVHT(CVHT);
};
window.deleteCongViecHoanThanh = deleteCongViecHoanThanh;
document.getElementById("two").addEventListener("click", function () {
  DSCV.sort((a, b) => {
    const jobA = a.job.toUpperCase();
    const jobB = b.job.toUpperCase();
    if (jobA < jobB) {
      return -1;
    }
    if (jobA > jobB) {
      return 1;
    } else {
      return 0;
    }
  });
  console.log(DSCV);
  renderArr(DSCV);
});
document.getElementById("three").addEventListener("click", function () {
  DSCV.sort((a, b) => {
    const jobA = a.job.toUpperCase();
    const jobB = b.job.toUpperCase();
    if (jobA < jobB) {
      return 1;
    }
    if (jobA > jobB) {
      return -1;
    } else {
      return 0;
    }
  });
  console.log(DSCV);
  renderArr(DSCV);
});
